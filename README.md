# RaksmeyAssignment

## Getting started

This project is for assignment CICD of DevOps Class, and it's followed below technology.
Now I'm seperate it by two part:

### This Application & Server using with Technology

```
Applicaiton:

Backend : NodeJs
Frontend : Express js
Database : Mariadb

Server:
OS : Aamazon Linux 2023
Service : Node (npm)
Port: 8080
```
### Server Side
-------------------------------------
### Step 1 : OS level with AWS Configure
```
- Make sure you have account with aws then create EC2 for Linux
- Make sure you have done on keypair (this one using for remote ssh)
```
### Step 2: Installation & Configuration
-------------------------------------
### Install Node
```
sudo yum install -y gcc-c++ make
curl -sL https://rpm.nodesource.com/setup_16.x | sudo -E bash -
sudo yum install -y nodejs
```
### Install Git
```
sudo yum install -y git
```
### Configure Systemd Service
`Service file : lotr.service`
```
sudo vim /etc/systemd/system/lotr.service
[Unit]
Description=RaksmeyAssigment
After=multi-user.target

[Service]
ExecStart=/usr/bin/node /home/ec2-user/raksmeyassignment/server.js
Restart=always
RestartSec=10
StandardOutput=syslog
StandardError=syslog
SyslogIdentifier=RaksmeyAssigment
User=ec2-user
EnvironmentFile=/home/ec2-user/RaksmeyAssigment/app.env

[Install]
WantedBy=multi-user.target
```
`Enable Service`
```
sudo systemctl enable lotr.service
sudo systemctl start lotr.service
```
### Install MariaDB 
```
sudo dnf update
sudo dnf install mariadb105-server
sudo systemctl start mariadb
sudo systemctl enable mariadb
sudo systemctl status mariadb
sudo mysql_secure_installation (Optional)

-- Accessing MariaDB
command : mysql -u root -p
you need to Enter the root password you set during the secure installation step.

Note: Create User to access: (this one run when you has been login into db sccessfully then create new user for app..recommand : do not use root user as user for app to access db)

CREATE USER 'new_user'@'localhost' IDENTIFIED BY 'password';
GRANT ALL PRIVILEGES ON database_name.* TO 'new_user'@'localhost';
FLUSH PRIVILEGES;
exit;

```

### Configure Nginx reverse proxy (Optional)
```
sudo amazon-linux-extras install nginx1 -y
sudo systemctl enable nginx
sudo systemctl start nginx
sudo vim /etc/nginx/nginx.conf
    location / {
          proxy_pass http://localhost:8080;
    }
sudo systemctl restart nginx
```
### Application Side
-------------------------------------
## Code
```
My source code in gitlab project : raksmeyAssigment

Here is some main file for running this app:

- server.js (this is nodejs)
- views/characters-page.ejs (this one for frontend showed using express js)
- gitlab-ci-yml (this file for allow gitlab to CICD)
```
## CICD
`File : .gitlab-ci.yml`
```
build:
    stage: build
    image: alpine:latest
    script:
        - echo "Building in prod... npm run build"
        - echo "Testing in prod... npm run test"

deploy:
    stage: deploy
    image: alpine:latest
    only:
        - main
    script:
        - apk add openssh-client
        - eval $(ssh-agent -s)
        - mkdir -p ~/.ssh/
        - chmod 600 ~/.ssh/
        - echo "SSH_KEY  => "$SSH_KEY
        - echo $SSH_KEY > ~/.ssh/id_rsa
        - echo $PROD_USER
        - echo $PROD_HOST
        - echo "$SSH_KEY" | tr -d '\r' | ssh-add -
        - |
          ssh -o StrictHostKeyChecking=no "$PROD_USER"@"$PROD_HOST" << EOF
          sudo git config --global --add safe.directory /home/ec2-user/raksmeyassignment
          sudo sh ~/raksmeyassignment/run.sh 
          sudo systemctl restart lotr.service
          sudo sh ~/raksmeyassignment/telegram_bot_build_stage.sh 
          echo "lotr.service Complete deployment..."
          echo "Complete deployment..."
          EOF 

    allow_failure: true
```
In this file , you will see some variables to be added here below is configure.

**Note:**
> $PROD_USER: is variable set up in CI/CD gitlab variable

> $PROD_HOST: is variable set up in CI/CD gitlab variable

> $SSH_KEY: is variable set up in CI/CD gitlab variable

-------------------------------------
![Image](https://gitlab.com/raksmeydara22/raksmeyassignment/-/raw/main/Image/Variable.png?ref_type=heads)

`File : run.sh`
```
#!/bin/bash
set -e

# Change to the project directory

cd /home/ec2-user/raksmeyassignment/

#sudo chmod 777 /home/ec2-user/raksmeyassignment/

# Pull the latest changes from the Git repository
echo "git pull...dd"
sudo git pull origin main  # Replace 'master' with your branch if it's different

echo "restart servce...ddd"
sudo systemctl restart lotr.service

# Install/update project dependencies using npm
sudo npm install --production

# Perform any additional build or update tasks here
sudo pm2 restart server

# Display a message indicating that the update is complete
echo "Node.js project updated and running in production mode!"
```

#### Result

-------------------------------------
![Image](https://gitlab.com/raksmeydara22/raksmeyassignment/-/raw/main/Image/Screenshot_2023-12-29_120421.png?ref_type=heads)

